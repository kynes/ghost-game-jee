<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>com.gitlab.kynes</groupId>
	<artifactId>ghost-game-jee</artifactId>
	<version>1.0-SNAPSHOT</version>
	<description>Ghost Game test webapp</description>
	<packaging>war</packaging>

	<properties>
		<failOnMissingWebXml>false</failOnMissingWebXml>
		<maven.compiler.target>1.8</maven.compiler.target>
		<maven.compiler.source>1.8</maven.compiler.source>

		<version.javaee_api>7.0</version.javaee_api>

		<version.guava>23.0</version.guava>
		<version.junit>4.12</version.junit>
		<version.jacoco>0.8.1</version.jacoco>
		<version.lombok>1.18.0</version.lombok>
		<version.assertj>3.10.0</version.assertj>
		<version.maven.war>3.1.0</version.maven.war>
		<version.bootstrap>4.1.1</version.bootstrap>
		<version.maven.compiler>3.7.0</version.maven.compiler>
		<version.arquillian.suite>1.2.0</version.arquillian.suite>
		<version.wildfly.plugin>1.2.1.Final</version.wildfly.plugin>
		<version.graphene.webdriver>2.3.2</version.graphene.webdriver>
		<version.arquillian_core>1.1.13.Final</version.arquillian_core>
		<version.arquillian_jacoco>1.0.0.Alpha9</version.arquillian_jacoco>
		<version.arquillian.chamelon>1.0.0.CR2</version.arquillian.chamelon>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.jboss.arquillian</groupId>
				<artifactId>arquillian-bom</artifactId>
				<version>${version.arquillian_core}</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency>
			<dependency>
				<groupId>com.google.guava</groupId>
				<artifactId>guava</artifactId>
				<version>${version.guava}</version>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>
		<!-- APIs -->
		<dependency>
			<groupId>javax</groupId>
			<artifactId>javaee-api</artifactId>
			<version>${version.javaee_api}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<version>${version.lombok}</version>
			<scope>provided</scope>
		</dependency>

		<!-- Web -->
		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>bootstrap</artifactId>
			<version>${version.bootstrap}</version>
		</dependency>

		<!-- Tests -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${version.junit}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.arquillian.container</groupId>
			<artifactId>arquillian-chameleon-junit-container-starter</artifactId>
			<version>${version.arquillian.chamelon}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.eu.ingwar.tools</groupId>
			<artifactId>arquillian-suite-extension</artifactId>
			<version>${version.arquillian.suite}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.jacoco</groupId>
			<artifactId>org.jacoco.core</artifactId>
			<version>${version.jacoco}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.jboss.arquillian.extension</groupId>
			<artifactId>arquillian-jacoco</artifactId>
			<version>${version.arquillian_jacoco}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.jboss.shrinkwrap.resolver</groupId>
			<artifactId>shrinkwrap-resolver-depchain</artifactId>
			<scope>test</scope>
			<type>pom</type>
		</dependency>
		<dependency>
			<groupId>org.assertj</groupId>
			<artifactId>assertj-core</artifactId>
			<version>${version.assertj}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.jboss.arquillian.graphene</groupId>
			<artifactId>graphene-webdriver</artifactId>
			<version>${version.graphene.webdriver}</version>
			<type>pom</type>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.jboss.arquillian.graphene</groupId>
			<artifactId>graphene-webdriver-impl</artifactId>
			<version>${version.graphene.webdriver}</version>
			<scope>test</scope>
		</dependency>

		<!-- Container -->
		<dependency>
			<groupId>org.wildfly.plugins</groupId>
			<artifactId>wildfly-maven-plugin</artifactId>
			<version>${version.wildfly.plugin}</version>
			<type>maven-plugin</type>
			<exclusions>
				<exclusion>
					<groupId>sun.jdk</groupId>
					<artifactId>jconsole</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
	</dependencies>

	<build>
		<finalName>${project.artifactId}</finalName>
		<plugins>
			<plugin>
				<groupId>org.wildfly.plugins</groupId>
				<artifactId>wildfly-maven-plugin</artifactId>
				<version>${version.wildfly.plugin}</version>
			</plugin>
			 <plugin>
              <groupId>org.owasp</groupId>
              <artifactId>dependency-check-maven</artifactId>
              <version>3.2.1</version>
              <executions>
                  <execution>
                      <goals>
                          <goal>check</goal>
                      </goals>
                  </execution>
              </executions>
            </plugin>
		</plugins>
	</build>


	<repositories>
		<repository>
			<id>jboss-public-repository-group</id>
			<name>JBoss Public Maven Repository Group</name>
			<url>https://repository.jboss.org/nexus/content/groups/public-jboss/</url>
			<layout>default</layout>
			<releases>
				<enabled>true</enabled>
				<updatePolicy>daily</updatePolicy>
			</releases>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>never</updatePolicy>
			</snapshots>
		</repository>
	</repositories>
</project>
