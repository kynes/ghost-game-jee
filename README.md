# Ghost Game 

## Description
This webapp tries to play as smart as posible the [Ghost Game](https://en.wikipedia.org/wiki/Ghost_(game)).

## How to run
To run the game:
```shell
mvn wildfly:run -Dmaven.test.skip=true
```

To execute test:
```shell
mvn test
```

In both cases, ports 8080, 8443 y 9990 should be free.

## Architecture
The choosen technical is:
1. [Java EE](https://github.com/javaee): Industrial Leading Standar composed of:
  * [Dependecy Injection](https://javax-inject.github.io/javax-inject/): Tool to allow Inversion of Control.
  * [EJB3](https://github.com/javaee/javax.ejb): Used to control beans used during server startup.
  * [JSF](https://github.com/javaserverfaces): Web components to build web pages.
1. Test: Tools choosen for testing:
  * [JUnit](https://junit.org/junit4/): Run test inside the IDE.
  * [Arquillian] (https://arquillian.org/): Easy replicate production enviroment starting a server for tests.
  * [AssertJ](https://joel-costigliola.github.io/assertj/): Framework to test fluently.
1. Other
  * [Lombok](https://projectlombok.org/): Remove boilerplate code.