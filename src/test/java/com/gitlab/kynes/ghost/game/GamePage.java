package com.gitlab.kynes.ghost.game;

import org.jboss.arquillian.graphene.Graphene;
import org.jboss.arquillian.graphene.page.Location;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Location( value = "game.xhtml" )
public class GamePage {
	
	@FindBy( id = "gameHeader" )
	private WebElement gameHeader;
	
	@FindBy( id = "gameTurn" )
	private WebElement gameTurn;
	
	@FindBy( id = "gameForm:inputLetter" )
	private WebElement input;
	
	@FindBy( id = "gameForm:playButton" )
	private WebElement playButton;
	
	public String getHeader() {
		return gameHeader.getText();
	}
	
	public String getTurn() {
		return gameTurn.getText();
	}

	public void setLetter( String letter ) {
		input.sendKeys( letter );
	}

	public void clickPlay() {
		Graphene.guardAjax( playButton ).click();
	}
}
