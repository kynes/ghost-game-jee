package com.gitlab.kynes.ghost.game;

import static org.assertj.core.api.Assertions.assertThat;

import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.graphene.page.InitialPage;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.gitlab.kynes.ghost.SuiteDeployment;

@RunAsClient
@RunWith( Arquillian.class )
public class GameViewTest extends SuiteDeployment {

	@Drone
	private WebDriver browser;
	
	@Test
	@InSequence( 1 )
	public void firstTurnHuman( @InitialPage GamePage gamePage ) {
		// Given
		// When
		// Then
		assertThat( gamePage.getHeader() ).isEqualTo( "Your turn" );
		assertThat( gamePage.getTurn() ).isEqualTo( "Turn 1" );
	}
	
	@Test
	@InSequence( 2 )
	public void playLetterAdvancesTurns( @InitialPage GamePage gamePage ) {
		// Given
		assertThat( gamePage.getTurn() ).isEqualTo( "Turn 1" );
		
		// When
		gamePage.setLetter( "a" );
		gamePage.clickPlay();
		
		// Then
		assertThat( gamePage.getTurn() ).isEqualTo( "Turn 3" );
	}
	
	@Test
	@InSequence( 3 )
	public void playerLooses( @InitialPage GamePage gamePage  ) {
		// Given
		assertThat( gamePage.getTurn() ).isEqualTo( "Turn 1" );

		// When
		gamePage.setLetter( "a" );
		gamePage.clickPlay();
		
		gamePage.setLetter( "b" );
		gamePage.clickPlay();
		
		gamePage.setLetter( "c" );
		gamePage.clickPlay();

		// Then
		assertThat( gamePage.getHeader() ).isEqualTo( "You loose!" );
	}
	
	@Test
	@InSequence( 4 )
	public void playerTriesToTrickInputValidation( @InitialPage GamePage gamePage  ) {
		// Given
		assertThat( gamePage.getTurn() ).isEqualTo( "Turn 1" );

		// When
		gamePage.setLetter( "a" );
		gamePage.clickPlay();
		
		gamePage.setLetter( "bc" );
		gamePage.clickPlay();

		// Then
		assertThat( gamePage.getHeader() ).isEqualTo( "Your turn" );
	}
	
	@Test
	@InSequence( 5 )
	public void playerWins( @InitialPage GamePage gamePage ) {
		// Given
		assertThat( gamePage.getTurn() ).isEqualTo( "Turn 1" );

		// When
		gamePage.setLetter( "b" );
		gamePage.clickPlay();
		
		gamePage.setLetter( "d" );
		gamePage.clickPlay();

		// Then
		assertThat( gamePage.getHeader() ).isEqualTo( "You win!" );
	}
}
