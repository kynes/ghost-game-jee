package com.gitlab.kynes.ghost.game;

import static org.assertj.core.api.Assertions.assertThat;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.gitlab.kynes.ghost.game.controller.GameService;
import com.gitlab.kynes.ghost.game.entity.TurnInfo;

@RunWith( Arquillian.class )
public class GameServiceTest {

	@Inject
	private GameService gameService;

	@Test
	@InSequence( 1 )
	public void initsCorrectly() {
		// Given
		// When
		// Then
		assertThat( gameService ).isNotNull();
	}

	@Test
	@InSequence( 2 )
	public void advanceOneTurn() {
		// Given
		// When
		TurnInfo turnInfo = gameService.playTurn( "a" );
		// Then
		assertThat( turnInfo.isGameFinised() ).isFalse();
		assertThat( turnInfo.getVirtualLetter() ).isEqualTo( "a" );
	}
	
	@Test
	@InSequence( 3 )
	public void advanceTwoTurns() {
		// Given
		// When
		TurnInfo turnInfo = gameService.playTurn( "aab" );
		// Then
		assertThat( turnInfo.isGameFinised() ).isFalse();
		assertThat( turnInfo.getVirtualLetter() ).isEqualTo( "a" );
	}
	
	@Test
	@InSequence( 4 )
	public void playerLooses() {
		// Given
		// When
		TurnInfo turnInfo = gameService.playTurn( "aabac" );
		// Then
		assertThat( turnInfo.isGameFinised() ).isTrue();
		assertThat( turnInfo.isPlayerWins() ).isFalse();
	}
	
	@Test
	@InSequence( 5 )
	public void playerWins() {
		// Given
		// When
		TurnInfo turnInfo = gameService.playTurn( "bcd" );
		// Then
		assertThat( turnInfo.isGameFinised() ).isTrue();
		assertThat( turnInfo.isPlayerWins() ).isTrue();
	}
}
