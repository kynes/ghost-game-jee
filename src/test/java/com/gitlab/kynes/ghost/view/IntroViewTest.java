package com.gitlab.kynes.ghost.view;

import static org.assertj.core.api.Assertions.assertThat;

import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.graphene.page.InitialPage;
import org.jboss.arquillian.graphene.page.Page;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.gitlab.kynes.ghost.SuiteDeployment;
import com.gitlab.kynes.ghost.game.GamePage;

@RunAsClient
@RunWith( Arquillian.class )
public class IntroViewTest extends SuiteDeployment {

	@Drone
	private WebDriver browser;
	
	@Page
	private GamePage gamePage;

	@Test
	@InSequence( 1 )
	public void introShown( @InitialPage IntroPage introPage ) {
		// Given
		// When
		// Then
		assertThat( introPage.getHeader() ).isEqualTo( "Ghost Game!" );
	}
	
	@Test
	@InSequence( 2 )
	public void playShowsGamePage( @InitialPage IntroPage introPage ) {
		// Given
		// When
		introPage.clickPlay();
		
		// Then
		assertThat( gamePage.getHeader() ).isEqualTo( "Your turn" );
	}

}
