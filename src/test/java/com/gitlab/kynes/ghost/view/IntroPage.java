package com.gitlab.kynes.ghost.view;

import org.jboss.arquillian.graphene.Graphene;
import org.jboss.arquillian.graphene.page.Location;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

@Location( value = "" )
public class IntroPage {
	@FindBy( id = "gameName" )
	private WebElement introHeader;

	@FindBy( id = "introForm:playButton" )
	private WebElement playButton;

	public String getHeader() {
		return introHeader.getText();
	}

	public void clickPlay() {
		Graphene.guardHttp( playButton ).click();
	}
}
