package com.gitlab.kynes.ghost.word;

import static com.gitlab.kynes.ghost.word.entity.Players.PLAYER_2;
import static org.assertj.core.api.Assertions.assertThat;

import javax.inject.Inject;

import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.gitlab.kynes.ghost.SuiteDeployment;
import com.gitlab.kynes.ghost.word.entity.LetterNode;

@RunWith( Arquillian.class )
public class WordsReaderTest extends SuiteDeployment {

	@Inject
	private LetterNode letters;

	@Test
	@InSequence( 1 )
	public void loadWords() {
		// Given
		// When
		// Then
		assertThat( letters ).isNotNull();
	}

	@Test
	@InSequence( 2 )
	public void firstWordCorrect() {
		// Given
		// When
		// Then
		assertThat( letters.containsWord(  "aabac" ) ).isTrue();
	}
	
	@Test
	@InSequence( 3 )
	public void lastWordCorrect() {
		// Given
		// When
		// Then
		assertThat( letters.containsWord(  "zyzzyvas" ) ).isTrue();
	}
	
	@Test
	@InSequence( 4 )
	public void nonexistantWordsNotFound() {
		// Given
		// When
		// Then
		assertThat( letters.containsWord(  "aaba" ) ).isFalse();
	}
	
	@Test
	@InSequence( 5 )
	public void firstWordMarkCorrect() {
		// Given
		LetterNode finalLetter = letters.getNextLetter( 'a' ).getNextLetter( 'a' ).getNextLetter( 'b' ).getNextLetter( 'a' ).getNextLetter( 'c' );
		
		// When
		// Then		
		assertThat( finalLetter.getTargetPlayer() ).isEqualByComparingTo( PLAYER_2 );
	}

	@Test
	@InSequence( 6 )
	public void getPaco() {
		// Given
		LetterNode letter = new LetterNode( 'a', null );
		
		// When
		LetterNode finalLetter = letter.addLetter( 'p' ).addLetter( 'a' ).addLetter( 'c' ).addLetter( 'o' );
		
		// Then		
		assertThat( finalLetter.getWord() ).isEqualTo( "paco" );
	}
}
