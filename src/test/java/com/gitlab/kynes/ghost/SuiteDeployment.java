package com.gitlab.kynes.ghost;

import static org.jboss.shrinkwrap.api.Filters.include;

import java.io.File;

import org.eu.ingwar.tools.arquillian.extension.suite.annotations.ArquillianSuiteDeployment;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;

import com.gitlab.kynes.ghost.app.Config;

@ArquillianSuiteDeployment
public class SuiteDeployment {

	private static final String WEBAPP_SRC = "src/main/webapp";

	@Deployment
	public static WebArchive createDeployment() {
		final File[] pomLibraries = Maven.resolver()
			.loadPomFromFile( "pom.xml" )
			.importRuntimeAndTestDependencies()
			.resolve()
			.withTransitivity()
			.asFile();

		WebArchive archive = ShrinkWrap.create( WebArchive.class, Config.APP_NAME + ".war" )
			.addPackages( true, "com.gitlab.kynes" )
			.addAsResource( Config.WORDS_FILE )
			.addAsLibraries( pomLibraries )
			.merge( getArchive(), "/", include( ".*\\.?html$" ) )
			.merge( getArchive(), "/", include( ".*\\.xml$" ) );
		return archive;
	}

	private static GenericArchive getArchive() {
		return ShrinkWrap.create( GenericArchive.class )
			.as( ExplodedImporter.class )
			.importDirectory( WEBAPP_SRC )
			.as( GenericArchive.class );
	}
}
