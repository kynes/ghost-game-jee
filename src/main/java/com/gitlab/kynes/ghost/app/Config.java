package com.gitlab.kynes.ghost.app;

public final class Config {
	private Config() {
	}

	public static final String APP_NAME = "ghostgame";
	public static final String WORDS_FILE = "gosthGameDict.txt";
}
