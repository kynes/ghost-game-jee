package com.gitlab.kynes.ghost.word.control;

public class Words {
	private Words() {
	}
	
	public static boolean sizeIsEven( String word ) {
		return ( word.length() & 1 ) == 0;
	}
}
