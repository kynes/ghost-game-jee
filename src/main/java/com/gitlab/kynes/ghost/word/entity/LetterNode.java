package com.gitlab.kynes.ghost.word.entity;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import lombok.Getter;
import lombok.Setter;

@Getter
public class LetterNode {

	private final LetterNode parent;
	private final Character letter;
	private final Map<Character, LetterNode> nextLetters;

	@Setter
	private String word;

	@Setter
	private Players targetPlayer;

	public LetterNode( Character letter, LetterNode parent ) {
		this.letter = letter;
		this.parent = parent;
		this.nextLetters = new HashMap<>();
	}

	public LetterNode addLetter( Character letter ) {
		LetterNode toAdd = new LetterNode( letter, this );

		nextLetters.put( letter, toAdd );

		return toAdd;
	}

	public boolean containsNextLetter( Character letter ) {
		return nextLetters.containsKey( letter );
	}

	public LetterNode getNextLetter( Character letter ) {
		return nextLetters.get( letter );
	}

	public boolean containsWord( String word ) {
		RecursiveResult result = recursiveFindLetter( word );

		return result.getFound() && result.getLetterFound()
			.isWholeWord();
	}

	public boolean invalidFragment( String word ) {
		RecursiveResult result = recursiveFindLetter( word );

		return !result.getFound();
	}

	public Optional<LetterNode> getFinalLetter( String word ) {
		RecursiveResult result = recursiveFindLetter( word );

		return result.getFound() ? Optional.ofNullable( result.getLetterFound() ) : Optional.empty();
	}

	@Override
	public String toString() {
		return getWord();
	}

	public String getWord() {
		if( word == null ) {
			return letter == null || parent == null ? "" : parent.getWord() + letter;
		}

		return word;
	}

	public boolean isWholeWord() {
		return word != null;
	}

	private RecursiveResult recursiveFindLetter( String word ) {
		RecursiveResult result = new RecursiveResult();
		
		if( word != null && !word.isEmpty() ) {
			AtomicBoolean found = new AtomicBoolean( true );
			
			AtomicReference<LetterNode> currentReference = new AtomicReference<>( this );
			
			word.chars()
			.mapToObj( c -> (char) c )
			.forEach( currentLetter -> {
				LetterNode referenceLetter = currentReference.get();
				
				if( referenceLetter.containsNextLetter( currentLetter ) ) {
					currentReference.set( referenceLetter.getNextLetter( currentLetter ) );
				} else {
					found.set( false );
				}
			} );
			
			result.setFound( found.get() );
			result.setLetterFound( currentReference.get() );
		}
		

		return result;
	}

	@Getter
	@Setter
	private class RecursiveResult {
		private Boolean found;
		private LetterNode letterFound;
	}
}