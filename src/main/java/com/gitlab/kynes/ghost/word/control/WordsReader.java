package com.gitlab.kynes.ghost.word.control;

import static com.gitlab.kynes.ghost.word.control.Words.sizeIsEven;
import static com.gitlab.kynes.ghost.word.entity.Players.PLAYER_1;
import static com.gitlab.kynes.ghost.word.entity.Players.PLAYER_2;
import static java.util.Comparator.reverseOrder;
import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;

import com.gitlab.kynes.ghost.app.Config;
import com.gitlab.kynes.ghost.word.entity.LetterNode;
import com.gitlab.kynes.ghost.word.entity.Players;

/**
 * Read Dictionary from file and loads into a Tree.
 */
@Startup
@Singleton
public class WordsReader {
	private LetterNode lettersTree;

	@Produces
	public LetterNode getLettersTree() {
		return lettersTree;
	}

	@PostConstruct
	public void init() {
		lettersTree = new LetterNode( null, null );

		Stream<String> wordsStream = new BufferedReader( new InputStreamReader( this.getClass()
			.getResourceAsStream( "/" + Config.WORDS_FILE ) ) ).lines();

		wordsStream.filter( Objects::nonNull )
			.forEachOrdered( this::insertWordInTree );

		List<LetterNode> leafs = new ArrayList<>();
		findLeafs( lettersTree, leafs );

		markLeafsForPlayers( leafs );

		markTreeForPlayers( lettersTree );
	}

	private void insertWordInTree( String word ) {
		AtomicReference<LetterNode> currentReference = new AtomicReference<>( lettersTree );

		word.chars()
			.mapToObj( c -> (char) c )
			.forEach( letter -> {
				LetterNode currentLetter = currentReference.get();

				currentLetter = currentLetter.containsNextLetter( letter ) ? currentLetter.getNextLetter( letter )
						: currentLetter.addLetter( letter );

				currentReference.set( currentLetter );
			} );
		
		currentReference.get().setWord( word );
	}

	private void findLeafs( LetterNode letter, List<LetterNode> leafs ) {
		if( letter.getNextLetters().isEmpty() ) {
			leafs.add( letter );
		} else {
			for( LetterNode nextLetter : letter.getNextLetters().values() ) {
				findLeafs( nextLetter, leafs ) ;
			}
		}
	}

	private void markLeafsForPlayers( List<LetterNode> leafs ) {
		leafs.stream()
			.forEach( currentLetter -> currentLetter.setTargetPlayer( sizeIsEven( currentLetter.getWord() ) ? PLAYER_1 : PLAYER_2 ) );
	}

	private void markTreeForPlayers( LetterNode letter ) {
		List<LetterNode> pendingLetters = findPendingLetters( letter );
		
		if( !pendingLetters.isEmpty() ) {
			pendingLetters.forEach( this::markTreeForPlayers );
		}
		
		Optional<Players> winningPlayer = findWinningPlayer( letter );
		
		winningPlayer.ifPresent( letter::setTargetPlayer );
	}

	private List<LetterNode> findPendingLetters( LetterNode letter ) {
		return letter.getNextLetters()
			.values()
			.stream()
			.filter( nextLetter -> nextLetter.getTargetPlayer() == null )
			.collect( toList() );
	}

	private Optional<Players> findWinningPlayer( LetterNode currentLetter ) {
		Map<Players, Long> statistics = currentLetter.getNextLetters()
			.values()
			.stream()
			.collect( groupingBy( LetterNode::getTargetPlayer, counting() ) );

		return statistics.entrySet()
			.stream()
			.sorted( comparingByValue( reverseOrder() ) )
			.findFirst()
			.map( Entry<Players, Long>::getKey );
	}
}
