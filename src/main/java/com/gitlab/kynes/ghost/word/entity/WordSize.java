package com.gitlab.kynes.ghost.word.entity;

public enum WordSize {
	INDIFFERENT, EVEN, ODD;
}
