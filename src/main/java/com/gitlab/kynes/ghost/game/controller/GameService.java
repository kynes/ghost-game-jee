package com.gitlab.kynes.ghost.game.controller;

import com.gitlab.kynes.ghost.game.entity.TurnInfo;

public interface GameService {

	/**
	 * Plays de player's turn and then the virtual opponent's turn, in both cases, if a word is spelled the oposing player is challenged. 
	 * 
	 * @param currentLetters Current playing letters
	 * @return Current turn and virtual opponent't turn info.
	 */
	TurnInfo playTurn( String currentLetters );

}
