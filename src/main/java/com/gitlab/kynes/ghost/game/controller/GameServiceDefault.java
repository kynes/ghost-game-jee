package com.gitlab.kynes.ghost.game.controller;

import static com.gitlab.kynes.ghost.word.entity.Players.PLAYER_2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collector;

import javax.ejb.Singleton;
import javax.inject.Inject;

import com.gitlab.kynes.ghost.game.entity.TurnInfo;
import com.gitlab.kynes.ghost.word.entity.LetterNode;

@Singleton
public class GameServiceDefault implements GameService, Serializable {
	private static final long serialVersionUID = 5571723415471760161L;
	private static final List<String> allLetters = Arrays.asList( "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"  );

	@Inject
	private LetterNode dictionary;

	private boolean gameFinished( String currentLetters ) {
		return wordExists( currentLetters ) || dictionary.invalidFragment( currentLetters );
	}

	private boolean wordExists( String currentLetters ) {
		return currentLetters.length() > 3 && dictionary.containsWord( currentLetters );
	}

	@Override
	public TurnInfo playTurn( String currentLetters ) {
		TurnInfo info = new TurnInfo();

		if( gameFinished( currentLetters ) ) {
			info.setGameFinised( true );
			info.setPlayerWins( false );
		} else {
			String virtualPlayerLetter = calculateNewLetter( currentLetters );
			info.setVirtualLetter( virtualPlayerLetter );

			String newLetters = currentLetters + virtualPlayerLetter;

			if( gameFinished( newLetters ) ) {
				info.setGameFinised( true );
				info.setPlayerWins( true );
			}
		}

		return info;
	}

	private String calculateNewLetter( String currentLetters ) {
		Optional<LetterNode> finalLetter = dictionary.getFinalLetter( currentLetters );
		
		return finalLetter.flatMap( this::getNextLetter ).orElseGet( () -> {
				Map<Character, LetterNode> nextLetters = finalLetter.get().getNextLetters();
				
				String nextLetter;
				
				if( nextLetters.isEmpty() ) {
					nextLetter = allLetters.stream().collect( randomItem() ).get();
				} else {
					nextLetter = nextLetters.entrySet().stream().collect( randomItem() ).get().getKey() + "";
				}
				
			return nextLetter;
		} );
	}
	
	private Optional<String> getNextLetter( LetterNode letter ) {
		return letter.getNextLetters()
			.entrySet()
			.stream()
			.filter( entry -> entry.getValue().getTargetPlayer() == PLAYER_2 )
			.collect( randomItem() )
			.map( car -> car.getKey().toString() );
	}	
	
	public static <T> Collector<T, List<T>, Optional<T>> randomItem() {
		final Random randomGenerator = new Random();
		
		return Collector.of( () -> (List<T>) new ArrayList<T>(), ( acc, elem ) -> acc.add( elem ), ( list1, list2 ) -> {
			final ArrayList<T> result = new ArrayList<>( list1 );
			result.addAll( list2 );
			return result;
		}, list -> list.isEmpty() ? Optional.empty() : Optional.of( list.get( randomGenerator.nextInt( list.size() ) ) ) );
	}
	
	
	
	
}
