package com.gitlab.kynes.ghost.game.boundary;

import java.io.Serializable;
import java.util.ArrayList;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.gitlab.kynes.ghost.game.controller.GameService;
import com.gitlab.kynes.ghost.game.entity.TurnInfo;

@Named
@ViewScoped
public class GameView implements Serializable {
	private static final long serialVersionUID = -7105682254228336219L;
	
	@Inject
	private Interaction interaction;

	@Inject
	private GameService gameService;
	
	public void beginGame() {
		interaction.setTurn( 1 );
		interaction.setHeader( "Your turn" );
		
		interaction.setLetters( new ArrayList<>() );
		interaction.setInputLetter( "" );
	}
	
	public void play() {
		advancePlayerTurn();
		
		String currentLetters = interaction.getWord();

		TurnInfo turnInfo = gameService.playTurn( currentLetters );
		
		if( turnInfo.getVirtualLetter() != null ) {
			interaction.getLetters().add( turnInfo.getVirtualLetter() );
		}
		
		if( turnInfo.isGameFinised() ) {
			interaction.setGameFinished( true );
			interaction.setPlayerWins( turnInfo.isPlayerWins() );
		}
		
		interaction.advanceTurn();
	}

	private void advancePlayerTurn() {
		interaction.getLetters().add( interaction.getInputLetter() );
		interaction.setInputLetter( "" );
		interaction.advanceTurn();
	}
	
}
