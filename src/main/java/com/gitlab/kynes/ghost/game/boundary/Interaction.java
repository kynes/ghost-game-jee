package com.gitlab.kynes.ghost.game.boundary;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
@Named
@ViewScoped
public class Interaction implements Serializable {
	private static final long serialVersionUID = -5386682107921739586L;

	private int turn;
	private String header;
	private List<String> letters;
	private boolean gameFinished;
	private boolean playerWins;
	
	@NotNull
	@Pattern( regexp = "^[a-zA-Z]$" )
	private String inputLetter;

	public String getWord() {
		return String.join( "", letters );
	}

	void advanceTurn() {
		setTurn( getTurn() + 1 );
	}
}
