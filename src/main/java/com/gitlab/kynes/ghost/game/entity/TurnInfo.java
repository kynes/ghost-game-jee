package com.gitlab.kynes.ghost.game.entity;

import lombok.Data;

@Data
public class TurnInfo {
	private String virtualLetter;
	private boolean gameFinised;
	private boolean playerWins;
}
